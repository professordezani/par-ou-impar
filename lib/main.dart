// ignore_for_file: prefer_const_constructors
// ignore_for_file: prefer_const_literals_to_create_immutables

import 'package:flutter/material.dart';
import 'dart:math';

void main() {
  runApp(ParImparApp());
}

class ParImparApp extends StatefulWidget
{
  @override
  State<ParImparApp> createState() => _ParImparAppState();
}

class _ParImparAppState extends State<ParImparApp> {

  int numero = 0;
  String status = '';

  void _play(String choice) {
    numero = Random().nextInt(10);
    if(numero % 2 == 0) {
      if(choice == 'par') {
        status = 'Você ganhou!';
      }
      else {
        status = 'Você perdeu!';
      }
    } else {
      if(choice == 'impar') {
        status = 'Você ganhou!';
      }
      else {
        status = 'Você perdeu!';
      }
    }

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Par ou Impar"),
        ),
        body: Column(
          children: [
            Expanded(
              child: Center(
                child: Text(
                  "$numero\n$status",
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              child: Row(
                children: [
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () => _play('par'),
                      child: Text("Par"),
                    ),
                  ),
                  Container(width: 10,),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () => _play('impar'),
                      child: Text("Impar"),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}